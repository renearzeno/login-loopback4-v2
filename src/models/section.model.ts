import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Coursde} from './coursde.model';

@model()
export class Section extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  Code: string;

  @property({
    type: 'string',
    required: true,
  })
  Name: string;

  @property({
    type: 'string',
    required: true,
  })
  Content: string;

  @property({
    type: 'string',
  })
  Video?: string;

  @property({
    type: 'string',
    required: true,
  })
  Attached: string;

  @belongsTo(() => Coursde)
  coursdeId: number;

  constructor(data?: Partial<Section>) {
    super(data);
  }
}

export interface SectionRelations {
  // describe navigational properties here
}

export type SectionWithRelations = Section & SectionRelations;
