import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Student} from './student.model';
import {Coursde} from './coursde.model';

@model()
export class Enroll extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'boolean',
    required: true,
  })
  approbedsection: boolean;

  @property({
    type: 'date',
    required: true,
  })
  startDate: string;

  @property({
    type: 'date',
    required: true,
  })
  finishDate: string;

  @belongsTo(() => Student)
  studentId: number;

  @belongsTo(() => Coursde)
  coursdeId: number;

  constructor(data?: Partial<Enroll>) {
    super(data);
  }
}

export interface EnrollRelations {
  // describe navigational properties here
}

export type EnrollWithRelations = Enroll & EnrollRelations;
