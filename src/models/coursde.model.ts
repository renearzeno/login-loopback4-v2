import {belongsTo, Entity, hasMany, model, property} from '@loopback/repository';
import {Area} from './area.model';
import {Faculty} from './faculty.model';
import {Section} from './section.model';
import {Enroll} from './enroll.model';

@model()
export class Coursde extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
    required: true,
  })
  professor: string;

  @property({
    type: 'number',
    default: 0,
  })
  rates?: number;

  @property({
    type: 'number',
    default: 1,
  })
  duration?: number;

  @property({
    type: 'string',
    required: true,
  })
  image: string;

  @belongsTo(() => Faculty)
  facultyId: number;

  @belongsTo(() => Area)
  areaId: number;

  @hasMany(() => Section)
  sections: Section[];

  @hasMany(() => Enroll)
  enrolls: Enroll[];

  constructor(data?: Partial<Coursde>) {
    super(data);
  }
}

export interface CoursdeRelations {
  // describe navigational properties here
}

export type CoursdeWithRelations = Coursde & CoursdeRelations;
