import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Coursde,
  Area,
} from '../models';
import {CoursdeRepository} from '../repositories';

export class CoursdeAreaController {
  constructor(
    @repository(CoursdeRepository)
    public coursdeRepository: CoursdeRepository,
  ) { }

  @get('/coursdes/{id}/area', {
    responses: {
      '200': {
        description: 'Area belonging to Coursde',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Area)},
          },
        },
      },
    },
  })
  async getArea(
    @param.path.number('id') id: typeof Coursde.prototype.id,
  ): Promise<Area> {
    return this.coursdeRepository.area(id);
  }
}
