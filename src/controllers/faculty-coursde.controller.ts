import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Faculty,
  Coursde,
} from '../models';
import {FacultyRepository} from '../repositories';

export class FacultyCoursdeController {
  constructor(
    @repository(FacultyRepository) protected facultyRepository: FacultyRepository,
  ) { }

  @get('/faculties/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Array of Faculty has many Coursde',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Coursde)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Coursde>,
  ): Promise<Coursde[]> {
    return this.facultyRepository.coursdes(id).find(filter);
  }

  @post('/faculties/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Faculty model instance',
        content: {'application/json': {schema: getModelSchemaRef(Coursde)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Faculty.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coursde, {
            title: 'NewCoursdeInFaculty',
            exclude: ['id'],
            optional: ['facultyId']
          }),
        },
      },
    }) coursde: Omit<Coursde, 'id'>,
  ): Promise<Coursde> {
    return this.facultyRepository.coursdes(id).create(coursde);
  }

  @patch('/faculties/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Faculty.Coursde PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coursde, {partial: true}),
        },
      },
    })
    coursde: Partial<Coursde>,
    @param.query.object('where', getWhereSchemaFor(Coursde)) where?: Where<Coursde>,
  ): Promise<Count> {
    return this.facultyRepository.coursdes(id).patch(coursde, where);
  }

  @del('/faculties/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Faculty.Coursde DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Coursde)) where?: Where<Coursde>,
  ): Promise<Count> {
    return this.facultyRepository.coursdes(id).delete(where);
  }
}
