import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Coursde,
  Enroll,
} from '../models';
import {CoursdeRepository} from '../repositories';

export class CoursdeEnrollController {
  constructor(
    @repository(CoursdeRepository) protected coursdeRepository: CoursdeRepository,
  ) { }

  @get('/coursdes/{id}/enrolls', {
    responses: {
      '200': {
        description: 'Array of Coursde has many Enroll',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Enroll)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Enroll>,
  ): Promise<Enroll[]> {
    return this.coursdeRepository.enrolls(id).find(filter);
  }

  @post('/coursdes/{id}/enrolls', {
    responses: {
      '200': {
        description: 'Coursde model instance',
        content: {'application/json': {schema: getModelSchemaRef(Enroll)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Coursde.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enroll, {
            title: 'NewEnrollInCoursde',
            exclude: ['id'],
            optional: ['coursdeId']
          }),
        },
      },
    }) enroll: Omit<Enroll, 'id'>,
  ): Promise<Enroll> {
    return this.coursdeRepository.enrolls(id).create(enroll);
  }

  @patch('/coursdes/{id}/enrolls', {
    responses: {
      '200': {
        description: 'Coursde.Enroll PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enroll, {partial: true}),
        },
      },
    })
    enroll: Partial<Enroll>,
    @param.query.object('where', getWhereSchemaFor(Enroll)) where?: Where<Enroll>,
  ): Promise<Count> {
    return this.coursdeRepository.enrolls(id).patch(enroll, where);
  }

  @del('/coursdes/{id}/enrolls', {
    responses: {
      '200': {
        description: 'Coursde.Enroll DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Enroll)) where?: Where<Enroll>,
  ): Promise<Count> {
    return this.coursdeRepository.enrolls(id).delete(where);
  }
}
