import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Coursde,
  Faculty,
} from '../models';
import {CoursdeRepository} from '../repositories';

export class CoursdeFacultyController {
  constructor(
    @repository(CoursdeRepository)
    public coursdeRepository: CoursdeRepository,
  ) { }

  @get('/coursdes/{id}/faculty', {
    responses: {
      '200': {
        description: 'Faculty belonging to Coursde',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Faculty)},
          },
        },
      },
    },
  })
  async getFaculty(
    @param.path.number('id') id: typeof Coursde.prototype.id,
  ): Promise<Faculty> {
    return this.coursdeRepository.faculty(id);
  }
}
