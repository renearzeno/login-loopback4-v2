import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Area,
  Coursde,
} from '../models';
import {AreaRepository} from '../repositories';

export class AreaCoursdeController {
  constructor(
    @repository(AreaRepository) protected areaRepository: AreaRepository,
  ) { }

  @get('/areas/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Array of Area has many Coursde',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Coursde)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Coursde>,
  ): Promise<Coursde[]> {
    return this.areaRepository.coursdes(id).find(filter);
  }

  @post('/areas/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Area model instance',
        content: {'application/json': {schema: getModelSchemaRef(Coursde)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Area.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coursde, {
            title: 'NewCoursdeInArea',
            exclude: ['id'],
            optional: ['areaId']
          }),
        },
      },
    }) coursde: Omit<Coursde, 'id'>,
  ): Promise<Coursde> {
    return this.areaRepository.coursdes(id).create(coursde);
  }

  @patch('/areas/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Area.Coursde PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Coursde, {partial: true}),
        },
      },
    })
    coursde: Partial<Coursde>,
    @param.query.object('where', getWhereSchemaFor(Coursde)) where?: Where<Coursde>,
  ): Promise<Count> {
    return this.areaRepository.coursdes(id).patch(coursde, where);
  }

  @del('/areas/{id}/coursdes', {
    responses: {
      '200': {
        description: 'Area.Coursde DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Coursde)) where?: Where<Coursde>,
  ): Promise<Count> {
    return this.areaRepository.coursdes(id).delete(where);
  }
}
