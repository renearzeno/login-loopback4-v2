import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Enroll,
  Coursde,
} from '../models';
import {EnrollRepository} from '../repositories';

export class EnrollCoursdeController {
  constructor(
    @repository(EnrollRepository)
    public enrollRepository: EnrollRepository,
  ) { }

  @get('/enrolls/{id}/coursde', {
    responses: {
      '200': {
        description: 'Coursde belonging to Enroll',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Coursde)},
          },
        },
      },
    },
  })
  async getCoursde(
    @param.path.number('id') id: typeof Enroll.prototype.id,
  ): Promise<Coursde> {
    return this.enrollRepository.coursde(id);
  }
}
