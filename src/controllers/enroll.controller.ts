import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Enroll} from '../models';
import {EnrollRepository} from '../repositories';

export class EnrollController {
  constructor(
    @repository(EnrollRepository)
    public enrollRepository : EnrollRepository,
  ) {}

  @post('/enrolls', {
    responses: {
      '200': {
        description: 'Enroll model instance',
        content: {'application/json': {schema: getModelSchemaRef(Enroll)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enroll, {
            title: 'NewEnroll',
            exclude: ['id'],
          }),
        },
      },
    })
    enroll: Omit<Enroll, 'id'>,
  ): Promise<Enroll> {
    return this.enrollRepository.create(enroll);
  }

  @get('/enrolls/count', {
    responses: {
      '200': {
        description: 'Enroll model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Enroll) where?: Where<Enroll>,
  ): Promise<Count> {
    return this.enrollRepository.count(where);
  }

  @get('/enrolls', {
    responses: {
      '200': {
        description: 'Array of Enroll model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Enroll, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Enroll) filter?: Filter<Enroll>,
  ): Promise<Enroll[]> {
    return this.enrollRepository.find(filter);
  }

  @patch('/enrolls', {
    responses: {
      '200': {
        description: 'Enroll PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enroll, {partial: true}),
        },
      },
    })
    enroll: Enroll,
    @param.where(Enroll) where?: Where<Enroll>,
  ): Promise<Count> {
    return this.enrollRepository.updateAll(enroll, where);
  }

  @get('/enrolls/{id}', {
    responses: {
      '200': {
        description: 'Enroll model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Enroll, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Enroll, {exclude: 'where'}) filter?: FilterExcludingWhere<Enroll>
  ): Promise<Enroll> {
    return this.enrollRepository.findById(id, filter);
  }

  @patch('/enrolls/{id}', {
    responses: {
      '204': {
        description: 'Enroll PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enroll, {partial: true}),
        },
      },
    })
    enroll: Enroll,
  ): Promise<void> {
    await this.enrollRepository.updateById(id, enroll);
  }

  @put('/enrolls/{id}', {
    responses: {
      '204': {
        description: 'Enroll PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() enroll: Enroll,
  ): Promise<void> {
    await this.enrollRepository.replaceById(id, enroll);
  }

  @del('/enrolls/{id}', {
    responses: {
      '204': {
        description: 'Enroll DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.enrollRepository.deleteById(id);
  }
}
