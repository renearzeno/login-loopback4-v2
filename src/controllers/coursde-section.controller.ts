import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Coursde,
  Section,
} from '../models';
import {CoursdeRepository} from '../repositories';

export class CoursdeSectionController {
  constructor(
    @repository(CoursdeRepository) protected coursdeRepository: CoursdeRepository,
  ) { }

  @get('/coursdes/{id}/sections', {
    responses: {
      '200': {
        description: 'Array of Coursde has many Section',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Section)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Section>,
  ): Promise<Section[]> {
    return this.coursdeRepository.sections(id).find(filter);
  }

  @post('/coursdes/{id}/sections', {
    responses: {
      '200': {
        description: 'Coursde model instance',
        content: {'application/json': {schema: getModelSchemaRef(Section)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Coursde.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Section, {
            title: 'NewSectionInCoursde',
            exclude: ['id'],
            optional: ['coursdeId']
          }),
        },
      },
    }) section: Omit<Section, 'id'>,
  ): Promise<Section> {
    return this.coursdeRepository.sections(id).create(section);
  }

  @patch('/coursdes/{id}/sections', {
    responses: {
      '200': {
        description: 'Coursde.Section PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Section, {partial: true}),
        },
      },
    })
    section: Partial<Section>,
    @param.query.object('where', getWhereSchemaFor(Section)) where?: Where<Section>,
  ): Promise<Count> {
    return this.coursdeRepository.sections(id).patch(section, where);
  }

  @del('/coursdes/{id}/sections', {
    responses: {
      '200': {
        description: 'Coursde.Section DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Section)) where?: Where<Section>,
  ): Promise<Count> {
    return this.coursdeRepository.sections(id).delete(where);
  }
}
