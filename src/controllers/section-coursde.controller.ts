import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Section,
  Coursde,
} from '../models';
import {SectionRepository} from '../repositories';

export class SectionCoursdeController {
  constructor(
    @repository(SectionRepository)
    public sectionRepository: SectionRepository,
  ) { }

  @get('/sections/{id}/coursde', {
    responses: {
      '200': {
        description: 'Coursde belonging to Section',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Coursde)},
          },
        },
      },
    },
  })
  async getCoursde(
    @param.path.number('id') id: typeof Section.prototype.id,
  ): Promise<Coursde> {
    return this.sectionRepository.coursde(id);
  }
}
