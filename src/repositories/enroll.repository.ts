import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Enroll, EnrollRelations, Student, Coursde} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {StudentRepository} from './student.repository';
import {CoursdeRepository} from './coursde.repository';

export class EnrollRepository extends DefaultCrudRepository<
  Enroll,
  typeof Enroll.prototype.id,
  EnrollRelations
> {

  public readonly student: BelongsToAccessor<Student, typeof Enroll.prototype.id>;

  public readonly coursde: BelongsToAccessor<Coursde, typeof Enroll.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('StudentRepository') protected studentRepositoryGetter: Getter<StudentRepository>, @repository.getter('CoursdeRepository') protected coursdeRepositoryGetter: Getter<CoursdeRepository>,
  ) {
    super(Enroll, dataSource);
    this.coursde = this.createBelongsToAccessorFor('coursde', coursdeRepositoryGetter,);
    this.registerInclusionResolver('coursde', this.coursde.inclusionResolver);
    this.student = this.createBelongsToAccessorFor('student', studentRepositoryGetter,);
    this.registerInclusionResolver('student', this.student.inclusionResolver);
  }
}
