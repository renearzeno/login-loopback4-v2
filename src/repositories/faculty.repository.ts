import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Faculty, FacultyRelations, Coursde} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {CoursdeRepository} from './coursde.repository';

export class FacultyRepository extends DefaultCrudRepository<
  Faculty,
  typeof Faculty.prototype.id,
  FacultyRelations
> {

  public readonly coursdes: HasManyRepositoryFactory<Coursde, typeof Faculty.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('CoursdeRepository') protected coursdeRepositoryGetter: Getter<CoursdeRepository>,
  ) {
    super(Faculty, dataSource);
    this.coursdes = this.createHasManyRepositoryFactoryFor('coursdes', coursdeRepositoryGetter,);
    this.registerInclusionResolver('coursdes', this.coursdes.inclusionResolver);
  }
}
