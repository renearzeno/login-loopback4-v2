import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {Coursde, CoursdeRelations, Faculty, Area, Section, Enroll} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {FacultyRepository} from './faculty.repository';
import {AreaRepository} from './area.repository';
import {SectionRepository} from './section.repository';
import {EnrollRepository} from './enroll.repository';

export class CoursdeRepository extends DefaultCrudRepository<
  Coursde,
  typeof Coursde.prototype.id,
  CoursdeRelations
> {

  public readonly faculty: BelongsToAccessor<Faculty, typeof Coursde.prototype.id>;

  public readonly area: BelongsToAccessor<Area, typeof Coursde.prototype.id>;

  public readonly sections: HasManyRepositoryFactory<Section, typeof Coursde.prototype.id>;

  public readonly enrolls: HasManyRepositoryFactory<Enroll, typeof Coursde.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('FacultyRepository') protected facultyRepositoryGetter: Getter<FacultyRepository>, @repository.getter('AreaRepository') protected areaRepositoryGetter: Getter<AreaRepository>, @repository.getter('SectionRepository') protected sectionRepositoryGetter: Getter<SectionRepository>, @repository.getter('EnrollRepository') protected enrollRepositoryGetter: Getter<EnrollRepository>,
  ) {
    super(Coursde, dataSource);
    this.enrolls = this.createHasManyRepositoryFactoryFor('enrolls', enrollRepositoryGetter,);
    this.registerInclusionResolver('enrolls', this.enrolls.inclusionResolver);
    this.sections = this.createHasManyRepositoryFactoryFor('sections', sectionRepositoryGetter,);
    this.registerInclusionResolver('sections', this.sections.inclusionResolver);
    this.area = this.createBelongsToAccessorFor('area', areaRepositoryGetter,);
    this.registerInclusionResolver('area', this.area.inclusionResolver);
    this.faculty = this.createBelongsToAccessorFor('faculty', facultyRepositoryGetter,);
    this.registerInclusionResolver('faculty', this.faculty.inclusionResolver);
  }
}
