import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Section, SectionRelations, Coursde} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {CoursdeRepository} from './coursde.repository';

export class SectionRepository extends DefaultCrudRepository<
  Section,
  typeof Section.prototype.id,
  SectionRelations
> {

  public readonly coursde: BelongsToAccessor<Coursde, typeof Section.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('CoursdeRepository') protected coursdeRepositoryGetter: Getter<CoursdeRepository>,
  ) {
    super(Section, dataSource);
    this.coursde = this.createBelongsToAccessorFor('coursde', coursdeRepositoryGetter,);
    this.registerInclusionResolver('coursde', this.coursde.inclusionResolver);
  }
}
