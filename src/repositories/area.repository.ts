import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Area, AreaRelations, Coursde} from '../models';
import {DbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {CoursdeRepository} from './coursde.repository';

export class AreaRepository extends DefaultCrudRepository<
  Area,
  typeof Area.prototype.id,
  AreaRelations
> {

  public readonly coursdes: HasManyRepositoryFactory<Coursde, typeof Area.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('CoursdeRepository') protected coursdeRepositoryGetter: Getter<CoursdeRepository>,
  ) {
    super(Area, dataSource);
    this.coursdes = this.createHasManyRepositoryFactoryFor('coursdes', coursdeRepositoryGetter,);
    this.registerInclusionResolver('coursdes', this.coursdes.inclusionResolver);
  }
}
